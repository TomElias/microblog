package com.microblog.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;
import com.microblog.Server;
import com.microblog.misc.ComparableTriplet;
import com.microblog.misc.TripletComparator;
import com.microblog.service.BlogService;

import junit.framework.TestSuite;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

public class BlogServiceTest extends TestSuite {

	private static RedisServer redis = null;
	private static Jedis client = null;
	private static Gson serializer = null;
	private static BlogService service = null;
	private static TreeSet<ComparableTriplet<Integer, Long>> topPosts = null;
	private static final TripletComparator comparator = new TripletComparator();
	
	public static final String COUNTER = "counter";
	
	@BeforeClass
	public static void setUp() {
		try {
			redis = new RedisServer(Server.REDIS_PORT);
			redis.start();
			client = new Jedis("localhost");
			client.set(COUNTER, "0");
			serializer = new Gson();
			topPosts = new TreeSet<ComparableTriplet<Integer, Long>>(comparator);
			
			service = new BlogService() {				
				@Override
				public Collection<ComparableTriplet<Integer, Long>> getTopPostsCollection() { return topPosts; }
				
				@Override
				public Gson getJSONSerializer() { return serializer; }
				
				@Override
				public Jedis getDataAccessClient() { return client; }
			};
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@AfterClass
	public static void shutDown() {
		if(redis != null) {
			redis.stop();
		}
	}
	
	@Before
	public void setUpTest() {
		//System.out.println("before test");
		// clean slate //
		service.getDataAccessClient().flushDB();
		service.getDataAccessClient().set(COUNTER, "0");
		topPosts = new TreeSet<ComparableTriplet<Integer, Long>>(comparator);
	}
	
	@After
	public void afterTest() {
		//System.out.println("after test");
	}
	
	@Test
	public void testCreatePost() {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		response = service.createNewPost("test post 2");
		assertTrue(response != null && response.equals("2"));
		
		response = service.createNewPost(null);
		assertNull(response);
		
		response = service.createNewPost("");
		assertNull(response);
	}
	
	@Test
	public void testGetPost() {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		response = service.createNewPost("test post 2");
		assertTrue(response != null && response.equals("2"));
		
		response = service.getPost(response);
		assertNotNull(response);
		assertTrue(response.equals("test post 2"));
		
		response = service.getPost("3");
		assertNull(response);		
	}
	
	@Test
	public void testUpdatePost() {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		boolean success = service.updatePost(response, "test post 2");
		assertTrue(success);
		
		success = service.updatePost("2", "test post 2");
		assertFalse(success);		
	}
	
	@Test
	public void testRemovePost() {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		boolean success = service.deletePost(response);
		assertTrue(success);
		
		response = service.getPost(response);
		assertNull(response);
		
		success = service.deletePost("1");
		assertFalse(success);		
	}
	
	@Test
	public void testUpvotePost() throws InterruptedException {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		boolean success = service.upvotePost(response, "tom");
		assertTrue(success);
		
		success = service.upvotePost(response, "tom");
		assertFalse(success);
		
		success = service.upvotePost("2", "tom");
		assertFalse(success);		
	}
	
	@Test
	public void testDownvotePost() {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		boolean success = service.downvotePost(response, "tom");
		assertTrue(success);
		
		success = service.downvotePost(response, "tom");
		assertFalse(success);
		
		success = service.downvotePost("2", "tom");
		assertFalse(success);		
	}
	
	@Test
	public void testTopPosts() throws InterruptedException {		
		String response = service.createNewPost("test post 1");
		assertTrue(response != null && response.equals("1"));
		
		service.upvotePost("1", "tom");
		service.upvotePost("1", "tom2");
		service.upvotePost("1", "tom3");
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 2");
		assertTrue(response != null && response.equals("2"));
		
		service.upvotePost("2", "tom");
		service.upvotePost("2", "tom2");
		service.upvotePost("2", "tom3");
		
		Thread.sleep(100);
				
		response = service.createNewPost("test post 3");
		assertTrue(response != null && response.equals("3"));
		
		service.upvotePost("3", "tom");
		service.upvotePost("3", "tom2");
		
		Thread.sleep(100);
						
		response = service.createNewPost("test post 4");
		assertTrue(response != null && response.equals("4"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 5");
		assertTrue(response != null && response.equals("5"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 6");
		assertTrue(response != null && response.equals("6"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 7");
		assertTrue(response != null && response.equals("7"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 8");
		assertTrue(response != null && response.equals("8"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 9");
		assertTrue(response != null && response.equals("9"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 10");
		assertTrue(response != null && response.equals("10"));
		
		Thread.sleep(100);
		
		response = service.createNewPost("test post 10");
		assertTrue(response != null && response.equals("11"));
		
		Thread.sleep(100);
				
		System.out.println(service.getTopPostsCollection().toString());
		
		Collection<?> topPosts = service.getTopPostsCollection();
		assertNotNull(topPosts);
		assertTrue(topPosts.size() == 10);
	}
}