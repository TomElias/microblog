package com.microblog.route;

import com.microblog.service.BlogService;

import redis.clients.jedis.Jedis;
import spark.Route;
import spark.Spark;
import static com.microblog.Server.logMessage;

import java.util.Collection;

import com.google.gson.Gson;
import com.microblog.Server;
import com.microblog.misc.ComparableTriplet;

/**
 * @author TE
 * a class containing all Spark Routes for the server.
 */
public class BlogRoutes {	
	private static BlogService impl = new BlogRoutes.BlogServiceImpl();
	
	public static final Route get = (req, res) -> {
		if(req.params("id") == null || req.params("id").length() == 0) return Spark.halt(404); // not found //
		
		String text = impl.getPost(req.params("id"));
		logMessage("GET - getting post " + req.params("id"));
		if(text == null) return Spark.halt(404); // not found //
		
		return text;
	};
	
	public static final Route post = (req, res) -> {
		if(req.body() == null || req.body().length() == 0) return Spark.halt(500, "Error - missing post body");
		
		logMessage("CREATE - creating post " + req.body());
		String id = impl.createNewPost(req.body());
		return id;
	};
	
	public static final Route update = (req, res) -> {
		String id = req.params("id");
		String text = req.body();
		
		if(id == null || id.length() == 0) return Spark.halt(404); // not found //
		if(text == null || text.length() == 0) return Spark.halt(500, "Error - missing post body");
		
		logMessage("UPDATE - updating post " + id + " with body: " + text);
		return impl.updatePost(id, text);
	};
	
	public static final Route remove = (req, res) -> {
		String id = req.params("id");
		
		if(id == null || id.length() == 0) return Spark.halt(404); // not found //
		
		logMessage("DELETE - deleting post " + id);
		return impl.deletePost(id);
	};
	
	public static final Route upvote = (req, res) -> {
		String id = req.params("id");
		String user = req.params("user");
		
		if(id == null || id.length() == 0) return Spark.halt(404); // not found //
		if(user == null || user.length() == 0) return Spark.halt(500, "Error - missing User name for upvoting.");
		
		logMessage("UPVOTE - upvoting post " + id + " by user: " + user);
		return impl.upvotePost(id, user);
	};
	
	public static final Route downvote = (req, res) -> {
		String id = req.params("id");
		String user = req.params("user");
		
		if(id == null || id.length() == 0) return Spark.halt(404); // not found //
		if(user == null || user.length() == 0) return Spark.halt(500, "Error - missing User name for downvoting.");
		
		logMessage("DOWNVOTE - downvoting post " + id + " by user: " + user);
		return impl.downvotePost(id, user);
	};
	
	public static final Route topposts = (req, res) -> {
		logMessage("TOPPOSTS - getting top posts");
		return impl.getTopPostsCollection();
	};
	
	private static class BlogServiceImpl extends BlogService { // dependency injection the old-fashioned way //

		@Override
		public Jedis getDataAccessClient() {
			return Server.JEDIS;
		}

		@Override
		public Gson getJSONSerializer() {
			return Server.GSON;
		}

		@Override
		public Collection<ComparableTriplet<Integer, Long>> getTopPostsCollection() {
			return Server.TOP_POSTS;
		}		
	} 
}