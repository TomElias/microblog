package com.microblog.service;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import com.google.gson.Gson;
import com.microblog.entity.BlogPost;
import com.microblog.misc.ComparableTriplet;

import redis.clients.jedis.Jedis;

/**
 * @author TE
 * Server logic is implemented here.
 */
public abstract class BlogService {
	public static final String COUNTER = "counter";
	private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock(); // unfair locking //

	// dependency injection through overriding //
	public abstract Jedis getDataAccessClient();
	public abstract Gson getJSONSerializer();
	public abstract Collection<ComparableTriplet<Integer, Long>> getTopPostsCollection();
		
	public String getPost(String id) {		
		ReadLock lock = readWriteLock.readLock();
		lock.lock();
		
		String result = null;
		String text = getDataAccessClient().get(id);
		if(text != null && !"".equals(text) && !"nil".equals(text)) {
			BlogPost blogPost = getJSONSerializer().fromJson(text, BlogPost.class);
			result = blogPost.getPostText();
		}
		
		lock.unlock();
		return result;
	}
	
	public String createNewPost(String text) {
		if(text == null || text.length() == 0) return null;
		
		WriteLock lock = readWriteLock.writeLock();
		lock.lock();
		
		Integer counter = Integer.parseInt(getDataAccessClient().get(COUNTER));
		counter++;
		BlogPost entry = new BlogPost(text);
		getDataAccessClient().set("" + counter, getJSONSerializer().toJson(entry));
		getDataAccessClient().set(COUNTER, getJSONSerializer().toJson(counter));
		
		getTopPostsCollection().remove(new ComparableTriplet<Integer, Long>(counter, 0, 0L));
		getTopPostsCollection().add(new ComparableTriplet<Integer, Long>(counter, /*entry.getUpvotingUsers().size()*/ 0, entry.getCreationDate().getTime()));
		if(getTopPostsCollection().size() == 11) {
			getTopPostsCollection().remove(getTopPostsCollection().iterator().next());
		}
		
		lock.unlock();
		return "" + counter;
	}
	
	public boolean updatePost(String postId, String text) {
		WriteLock lock = readWriteLock.writeLock();
		lock.lock();
		
		boolean success = true;
		String postJSON = getDataAccessClient().get(postId);
		if(postJSON != null && !"".equals(postJSON)) {
			BlogPost blogPost = getJSONSerializer().fromJson(postJSON, BlogPost.class);
			blogPost.setPostText(text);
			blogPost.setUpdateDate(new Date());
			getDataAccessClient().set(postId, getJSONSerializer().toJson(blogPost));
		} else {
			success = false;
		}
		
		lock.unlock();
		
		return success;		
	}
	
	public boolean deletePost(String postId) {
		WriteLock lock = readWriteLock.writeLock();
		lock.lock();
		
		Long result = getDataAccessClient().del(postId);
		getTopPostsCollection().remove(new ComparableTriplet<Integer, Long>(Integer.parseInt(postId), null, null));
		
		lock.unlock();
		
		return  result == 1;
	}
	
	public boolean upvotePost(String id, String userId) throws InterruptedException {
		WriteLock lock = readWriteLock.writeLock();
		lock.lock();
		
		boolean success = true;
		String postJSON = getDataAccessClient().get(id);
		if(postJSON != null && !"".equals(postJSON)) {
			BlogPost blogPost = getJSONSerializer().fromJson(postJSON, BlogPost.class);
			success = blogPost.getUpvotingUsers().add(userId); // false if exists //
			
			if(success) {
				getDataAccessClient().set(id, getJSONSerializer().toJson(blogPost));
				
				getTopPostsCollection().remove(new ComparableTriplet<Integer, Long>(Integer.parseInt(id), 0, 0L));
				Thread.sleep(150); // with 100ms it fails randomly! //
				getTopPostsCollection().add(new ComparableTriplet<Integer, Long>(Integer.parseInt(id), blogPost.getUpvotingUsers().size(), blogPost.getCreationDate().getTime()));
				if(getTopPostsCollection().size() == 11) {
					getTopPostsCollection().remove(getTopPostsCollection().iterator().next());
				}
			}
		} else {
			success = false; 
		}
		
		lock.unlock();
		//System.out.println(getTopPostsCollection().toString());
		return success;
	}
	
	public boolean downvotePost(String id, String userId) {
		WriteLock lock = readWriteLock.writeLock();
		lock.lock();
		
		boolean success = true;
		String postJSON = getDataAccessClient().get(id);
		if(postJSON != null && !"".equals(postJSON)) {
			BlogPost blogPost = getJSONSerializer().fromJson(postJSON, BlogPost.class);
			success = blogPost.getDownvotingUsers().add(userId); // false if exists //
			
			if(success) {
				getDataAccessClient().set(id, getJSONSerializer().toJson(blogPost));
			}
		} else {
			success = false; 
		}
		
		lock.unlock();
		return success;
	}	
}