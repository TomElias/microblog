package com.microblog;

import static spark.Spark.before;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.internalServerError;
import static spark.Spark.notFound;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.port;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import com.google.gson.Gson;
import com.google.common.collect.*;
import com.microblog.misc.ComparableTriplet;
import com.microblog.misc.TripletComparator;
import com.microblog.route.BlogRoutes;

import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

/**
 * @author TE
 * A Standalone server built with Spark that offers REST API for managing the microblog.
 */
public class Server {
	// a log for the entire server //
	private static final Logger SERVER_LOG = Logger.getLogger(Server.class.getName());
	public static RedisServer REDIS_SERVER = null;
	public static Jedis JEDIS = null;
	public static final Gson GSON = new Gson();
	public static final Integer REDIS_PORT = 6379;
	public static Collection<ComparableTriplet<Integer, Long>> TOP_POSTS = new HashSet<ComparableTriplet<Integer, Long>>(); //<ComparableTriplet<Integer, Long>>(10, new PairComparator());
	

	public static void logMessage(String message) {
		SERVER_LOG.info(String.format("%s : %s", LocalDateTime.now(), message));
	}

	public static void main(String[] args) {
		try {
			// set up a logging object //
			SERVER_LOG.setLevel(Level.INFO);
			FileHandler fh = new FileHandler("server.log");
			System.setProperty("java.util.logging.SimpleFormatter.format", "%5$s%n");
			fh.setFormatter(new SimpleFormatter());
			SERVER_LOG.addHandler(fh);
			
			logMessage("BOOT - Boot Starting.");

			// init embedded redis //
			logMessage("BOOT - Initializing embedded Redis...");
			
			// are we using an external Redis? //
			if(args != null && args.length > 1 && args[1] != null && !"".equals(args[1])) {
				logMessage("BOOT - found path to external Redis: " + args[1]);
				REDIS_SERVER = new RedisServer(new File(args[1]), REDIS_PORT);
			} else { // no we are not... //
				REDIS_SERVER = new RedisServer(REDIS_PORT);
			}
			
			// start Redis //
			REDIS_SERVER.start();
			
			// init Jedis client //
			JEDIS = new Jedis("localhost");
			JEDIS.set("counter", "0"); // place a counter to track the number of blog posts, used to assign post with IDs //
			
			logMessage("BOOT - Done.");

			Integer port = 8080;
			if(args != null && args.length > 0 && args[0] != null && !"".equals(args[0])) {
				port = Integer.parseInt(args[0]);
			}
			
			// map the routes to URLs //
			logMessage("BOOT - Setting Spark routing on port: " + port);		
			
			port(port);
			get("/", (req, res) -> "Welcome to the MicroBlog!");
			path("/blog", () -> {
				before("/*", (req, res) -> logMessage("incoming request: " + req.pathInfo()));				
				get("/read/:id", BlogRoutes.get);
				post("/post", BlogRoutes.post);
				put("/update/:id", BlogRoutes.update);
				delete("/remove/:id", BlogRoutes.remove);
				post("/upvote/:id/:user", BlogRoutes.upvote);
				post("/downvote/:id/:user", BlogRoutes.downvote);
				get("/topposts", BlogRoutes.topposts);
			});
			
			internalServerError((req, res) -> "Internal Server Error.");
			notFound((req, res) -> "URL mapping not found."); 

			logMessage("BOOT - Done.");

			// on graceful shutdown - perform shutdown of embedded Redis //
			logMessage("BOOT - Setting shutdown hook...");
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					Server.logMessage("SHUTDOWN - stopping Redis...");
					Server.REDIS_SERVER.stop();	
					Server.logMessage("SHUTDOWN - Done.");
				}
			});

			logMessage("BOOT - Boot complete.");
		} catch (Throwable exc) {
			logMessage("BOOT - Boot failed with exception: " + exc.getLocalizedMessage());
			exc.printStackTrace();
		}
	}	
}