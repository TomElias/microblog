package com.microblog.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author TE
 * POJO entity representing a single blog post.
 */
public class BlogPost {
	private String postText;
	private Date creationDate = new Date();
	private Date updateDate = null;
	private Set<String> upvotingUsers = new HashSet<String>();
	private Set<String> downvotingUsers = new HashSet<String>();
	
	public BlogPost(String text) {
		this.postText = text;
	}

	public String getPostText() {
		return postText;
	}

	public void setPostText(String postText) {
		this.postText = postText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Set<String> getUpvotingUsers() {
		return upvotingUsers;
	}

	public void setUpvotingUsers(Set<String> upvotingUsers) {
		this.upvotingUsers = upvotingUsers;
	}

	public Set<String> getDownvotingUsers() {
		return downvotingUsers;
	}

	public void setDownvotingUsers(Set<String> downvotingUsers) {
		this.downvotingUsers = downvotingUsers;
	}
}