package com.microblog.misc;

/**
 * @author TE
 * A wrapper object used for sorting in the priority queue. it contains the ID of the blog post, the number of upvotes as the first element and the creation date (epoch time) as the second element.
 * @param <S> usually Integer, the number of upvotes.
 * @param <T> usually Long, the epoch time in milliseconds.
 */
public class ComparableTriplet<S extends Comparable<S>, T extends Comparable<T>> implements Comparable<ComparableTriplet<S, T>> {
	private Integer id;
	private S first;
	private T second;
	
	public ComparableTriplet(Integer id, S item1, T item2) {
		this.id = id;
		this.first = item1;
		this.second = item2;
	}
	
	public Integer getId() {
		return this.id;
	}

	public S getFirst() {
		return first;
	}

	public void setFirst(S first) {
		this.first = first;
	}

	public T getSecond() {
		return second;
	}

	public void setSecond(T second) {
		this.second = second;
	}

	/**
	 * compare the number of upvotes first, and if they are equal, the creation date as millis.
	 */
	@Override
	public int compareTo(ComparableTriplet<S, T> other) {
		if(this.id == other.getId()) return 0;
		
		int compareFirst = first.compareTo(other.getFirst());
		if(compareFirst != 0) return compareFirst;
		
		int comapreSecond = second.compareTo(other.getSecond());
		return comapreSecond;
	}
	
	/**
	 * two triplets represent the same BlogPost if they have the same ID.
	 */
	@Override
	public boolean equals(Object obj) {
		ComparableTriplet other = (ComparableTriplet)obj;
		return this.id == other.getId();
	}
	
	@Override
	public int hashCode() {
		return this.id;
	}
	
	@Override
	public String toString() {
		return "id: " + getId() + " upvotes: " + getFirst() + " creation time: " + getSecond();
	}
}