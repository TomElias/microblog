package com.microblog.misc;

import java.util.Comparator;

public class TripletComparator implements Comparator<ComparableTriplet<? extends Comparable, ? extends Comparable>> {

	@Override
	public int compare(ComparableTriplet o1, ComparableTriplet o2) {
		return o1.compareTo(o2);
	}
}
